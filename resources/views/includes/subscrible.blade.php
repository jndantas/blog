    <!-- Newsletter Section-->
    <section class="newsletter no-padding-top">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2>Novas por Email!</h2>
              <p class="text-big">Inscreva-se para obter novos conteúdos, atualizações, pesquisas e ofertas do {{ $settings->site_name }}.</p>
            </div>
            <div class="col-md-8">
              <div class="form-holder">
                <form action="{{ route('subscribe')}}" method="POST">
                    {{ csrf_field()}}
                  <div class="form-group">
                    <input type="email" name="email" id="email" placeholder="Seu Email!" required>
                    <button type="submit" class="submit">inscreva</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
