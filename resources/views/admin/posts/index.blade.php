@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Posts</h1>
</div>


<div class="box">
    <div class="box-header">
        <div class="pull-right">
                <a href="{{ route('post.create') }}" class="btn btn-primary">Novo</a>
        </div>
    </div>

    <div class="box-body">
<div class="panel panel-default">

    <table id="postsTable" class="table table-striped table-bordered">
        <thead>
            <th>Imagem</th>
            <th>Título</th>
            <th>Editar</th>
            <th>Apagar</th>
        </thead>
        <tbody>
            @if ($posts->count() > 0)
                @foreach ($posts as $p)
                    <tr>
                        <td><img src="{{ $p->featured }}" alt="{{ $p->title }}" width=90px height=50px></td>
                        <td>{{ $p->title }}</td>
                        <td><a href="{{ route('post.edit', ['id' => $p->id ]) }}" class="btn btn-xs btn-info"><i class="fas fa-pencil-alt"></i></a></td>
                        <td><a href="{{ route('post.delete', ['id' => $p->id ]) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach
            @else
            <th colspan="5" class="text-center">Sem posts publicados</th>
            @endif
        </tbody>
    </table>
</div>
</div>
</div>

@section('js')
<script>
    $(document).ready(function() {
        $('#postsTable').dataTable( {
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
                    }
                });
                });
    </script>
@stop


@stop
