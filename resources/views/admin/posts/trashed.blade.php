@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Posts Apagados</h1>
</div>

<div class="box">
    <div class="box-header">

    </div>

    <div class="box-body">
<div class="panel panel-default">

    <table id="postsTrashedTable" class="table table-striped table-bordered">
        <thead>
            <th>Imagem</th>
            <th>Título</th>
            <th>Editar</th>
            <th>Recuperar</th>
            <th>Excluir Permanentemente</th>
        </thead>
        <tbody>
            @if ($posts->count() > 0)

                @foreach ($posts as $p)
                    <tr>
                        <td><img src="{{ $p->featured }}" alt="{{ $p->title }}" width=90px height=50px></td>
                        <td>{{ $p->title }}</td>
                        <td><a href="{{ route('post.edit', ['id' => $p->id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td><a href="{{ route('post.restore', ['id' => $p->id ]) }}" class="btn btn-xs btn-success"><i class="fa fa-recycle" aria-hidden="true"></i></a></td>
                        <td><a href="{{ route('post.kill', ['id' => $p->id ]) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach

            @else
                    <tr>
                        <th colspan="5" class="text-center">Sem posts deletados</th>
                    </tr>
            @endif
        </tbody>
    </table>
</div>
</div>
</div>

@section('js')
<script>
    $(document).ready(function() {
        $('#postsTrashedTable').dataTable( {
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
                    }
                });
                });
    </script>
@stop


@stop
