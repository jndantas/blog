@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Novo Post</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" name="title" id="" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="featured">Imagem</label>
                        <input type="file" name="featured" id="" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="category">Selecione uma Categoria</label>
                        <select name="category_id" id="category" class="form-control">
                            @foreach ($categories as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="tags">Selcione tags</label>
                        @foreach ($tags as $t)

                        <div class="checkbox">
                            <label><input type="checkbox" name="tags[]" value="{{ $t->id }}">{{ $t->tag }}</label>
                        </div>

                        @endforeach
                    </div>

                    <div class="form-group">
                        <label for="content">Conteúdo</label>
                        <textarea name="content" id="content" cols="5" rows="5" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @section('css')
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
        @stop

        @section('js')
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
        <script>
        $(document).ready(function() {
            $('#content').summernote();
        });
        </script>
        @stop

@endsection
