@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar post: {{ $post->title }}</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ route('post.update', ['id' => $post->id]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" name="title" id="" class="form-control" value="{{$post->title}}">
                    </div>

                    <div class="form-group">
                        <label for="featured">Imagem</label>
                        <input type="file" name="featured" id="" class="form-control" value>
                    </div>

                    <div class="form-group">
                        <label for="category">Selecione uma Categoria</label>
                        <select name="category_id" id="category" class="form-control">
                            @foreach ($categories as $c)
                                <option value="{{ $c->id }}"
                                @if ($post->category_id == $c->id)
                                    selected
                                @endif

                                >{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                            <label for="tags">Selcione tags</label>
                            @foreach ($tags as $tag)

                            <div class="checkbox">
                                <label><input type="checkbox" name="tags[]" value="{{ $tag->id }}"
                                    @foreach ($post->tags as $t)
                                        @if ($t->id == $t->id)
                                            checked
                                        @endif

                                    @endforeach
                                    >{{ $tag->tag }}</label>
                            </div>

                            @endforeach
                        </div>

                    <div class="form-group">
                        <label for="content">Conteúdo</label>
                        <textarea name="content" id="content" cols="5" rows="5" class="form-control">{{ $post->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Atualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection

@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@stop

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script>
$(document).ready(function() {
    $('#content').summernote();
});
</script>
@stop
