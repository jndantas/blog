@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar Tag : {{ $tag->tag }}
        </h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">
            <div class="panel-heading">
                Editar Tag : {{ $tag->tag }}
            </div>

            <div class="panel-body">
                <form action="{{ route('tag.update', ['id' => $tag->id]) }}" method="post" >
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Tag</label>
                        <input type="text" name="tag" id="" value="{{ $tag->tag }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
