@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tags</h1>
</div>

<div class="box">
    <div class="box-header">
        <div class="pull-right">
                <a href="{{ route('tag.create') }}" class="btn btn-block btn-primary btn-flat">Novo</a>
        </div>
    </div>

    <div class="box-body">
<div class="panel panel-default">
    <div class="panel-heading">Tags</div>

    <table id="tagTable" class="table table-striped table-bordered">
        <thead>
            <th>Nome</th>
            <th>Editar</th>
            <th>Apagar</th>
        </thead>
        <tbody>
            @if ($tags->count() > 0)
                @foreach ($tags as $t)
                <tr>
                    <td>{{ $t->tag }}</td>
                    <td><a href="{{ route('tag.edit', ['id' => $t->id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><a href="{{ route('tag.delete', ['id' => $t->id ]) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                </tr>
                @endforeach
            @else
                <th colspan="5" class="text-center">Sem tags cadastradas</th>
            @endif
        </tbody>
    </table>
</div>
</div>
</div>

@section('js')
<script>
    $(document).ready(function() {
        $('#tagTable').dataTable( {
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
                    }
                });
                });
    </script>
@stop


@stop
