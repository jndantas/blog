@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar configurações do Blog</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">

            <div class="panel-body">
                <form action="{{ route('settings.update') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Nome do Site</label>
                        <input type="text" name="site_name" value="{{ $settings->site_name }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">País/Estado</label>
                        <input type="text" name="country" value="{{ $settings->country }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Endereço</label>
                        <input type="text" name="address" value="{{ $settings->address }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Telefone para contato</label>
                        <input type="text" name="contact_number" value="{{ $settings->contact_number }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Email para contato</label>
                        <input type="text" name="contact_email" value="{{ $settings->contact_email }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Disponível</label>
                        <input type="text" name="disponible" value="{{ $settings->disponible }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Suporte</label>
                        <input type="text" name="support" value="{{ $settings->support }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Sobre o Site</label>
                        <textarea name="about" id="about" cols="5" rows="5" class="form-control">{{ $settings->about }}</textarea>
                    </div>


                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Atualizar configurações do Site
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
