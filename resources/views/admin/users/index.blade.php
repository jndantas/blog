@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Usuários</h1>
</div>

<div class="box">
    <div class="box-header">
        <div class="pull-right">
                <a href="{{ route('user.create') }}" class="btn btn-block btn-primary btn-flat">Novo</a>
        </div>
    </div>

    <div class="box-body">
<div class="panel panel-default">

    <table id="usersTable" class="table table-striped table-bordered">
        <thead>
            <th>Imagem</th>
            <th>Nome</th>
            <th>Permissões</th>
            <th>Apagar</th>
        </thead>
        <tbody>
            @if ($users->count() > 0)
                @foreach ($users as $u)
                    <tr>
                        <td><img src="{{ asset($u->profile->avatar) }}" alt="" width="60px" height="60px" style="border-radius: 50%;"></td>
                        <td>{{ $u->name }}</td>
                        <td> @if ($u->admin)
                            <a href="{{ route('user.not.admin', ['id' => $u->id]) }}" class="btn btn-xs btn-danger">Remover Permissões</a>
                            @else
                        <a href="{{ route('user.admin', ['id' => $u->id]) }}" class="btn btn-xs btn-success">Torne Administrador</a>
                        @endif </td>
                        <td>
                            @if (Auth::id() !== $u->id)
                            <a href="{{ route('user.delete', ['id' => $u->id]) }}" class="btn btn-xs btn-danger">Apagar</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
            <th colspan="5" class="text-center">Sem usuários cadastrados</th>
            @endif
        </tbody>
    </table>
</div>
</div>
</div>

@section('js')
<script>
    $(document).ready(function() {
        $('#usersTable').dataTable( {
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
                    }
                });
                });
    </script>
@stop


@stop
