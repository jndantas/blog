@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar seu perfil</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">

            <div class="panel-body">
                <form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" name="name" id="" value="{{ $user->name }}" class="form-control">
                    </div>
                    <div class="form-group">
                            <label for="name">Email</label>
                            <input type="email" name="email" id="" value="{{ $user->email }}"class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Nova Senha</label>
                        <input type="password" name="password" id="" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Upload seu Avatar</label>
                        <input type="file" name="avatar" id="" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Perfil do Facebook</label>
                        <input type="text" name="facebook" id="" value="{{ $user->profile->facebook }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Perfil do Youtube</label>
                        <input type="text" name="youtube" id="" value="{{ $user->profile->youtube }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Perfil do Twitter</label>
                        <input type="text" name="twitter" id="" value="{{ $user->profile->twitter }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Perfil do Instagram</label>
                        <input type="text" name="instagram" id="" value="{{ $user->profile->instagram }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="name">Sobre Você</label>
                        <textarea name="about" id="about" cols="5" rows="5" class="form-control">{{ $user->profile->about }}</textarea>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Atualizar Perfil
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
