@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Criar novo usuário</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">

            <div class="panel-body">
                <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" name="name" id="" class="form-control">
                    </div>
                    <div class="form-group">
                            <label for="name">Email</label>
                            <input type="email" name="email" id="" class="form-control">
                        </div>



                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
