@extends('layouts.master')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar categoria</h1>
</div>

@include('admin.includes.errors')

        <div class="panel panel-default">
            <div class="panel-heading">
                Editar categoria
            </div>

            <div class="panel-body">
                <form action="{{ route('category.update', ['id' => $category->id]) }}" method="post" >
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Nome</label>
                        <input type="text" name="name" id="" value="{{ $category->name }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">
                                Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
