<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('blog/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('blog/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="{{ asset('blog/css/fontastic.css') }}">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="{{ asset('blog/vendor/@fancyapps/fancybox/jquery.fancybox.min.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('blog/css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('blog/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">

    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>

    @include('layouts.headerblog')

    <!-- Hero Section-->
    <section style="background: url(../blog/img/hero.jpg); background-size: cover; background-position: center center" class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <h1>Bootstrap 4 Blog - A free template by Bootstrap Temple</h1><a href="#" class="hero-link">Discover More</a>
            </div>
            </div><a href=".intro" class="continue link-scroll"><i class="fa fa-long-arrow-down"></i> Scroll Down</a>
        </div>
    </section>


    <!-- Intro Section-->
    <section class="intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="h3">Some great intro here</h2>
                    <p class="text-big">Place a nice <strong>introduction</strong> here <strong>to catch reader's attention</strong>. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                </div>
            </div>
        </div>
    </section>


    <section class="featured-posts no-padding-top">
        <div class="container">
        <!-- Post-->
            <div class="row d-flex align-items-stretch">
                <div class="text col-lg-7">
                    <div class="text-inner d-flex align-items-center">
                        <div class="content">
                            <header class="post-header">
                                <div class="category">
                                    <a href="{{ route('category.single', ['id' => $first_post->category->id ]) }}">{{ $first_post->category->name }}</a>
                                </div>
                                    <a href="{{ route('post.single', ['slug' => $first_post->slug ])}}"> <h2 class="h4">{{ $first_post->title }}</h2></a>
                            </header>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                            <footer class="post-footer d-flex align-items-center">
                                <a href="#" class="author d-flex align-items-center flex-wrap">
                                    <div class="avatar"><img src="{{ asset($first_post->user->profile->avatar)}}" alt="{{ $first_post->user->name }}" class="img-fluid"></div>
                                    <div class="title"><span>{{ $first_post->user->name }}</span></div>
                                </a>
                                <div class="date"><i class="icon-clock"></i>{{ $first_post->created_at->diffForHumans() }}</div>
                                <div class="comments"><i class="icon-comment"></i>12</div>
                            </footer>
                        </div>
                    </div>
                </div>
                <div class="image col-lg-5"><img src="{{ $first_post->featured }}" alt="{{ $first_post->title }}"></div>
            </div>


            <!-- Post-->
            <div class="row d-flex align-items-stretch">
                    <div class="image col-lg-5"><img src="{{ $second_post->featured }}" alt="{{ $second_post->title }}"></div>
                    <div class="text col-lg-7">
                      <div class="text-inner d-flex align-items-center">
                        <div class="content">
                          <header class="post-header">
                            <div class="category"><a href="{{ route('category.single', ['id' => $second_post->category->id ]) }}">{{ $second_post->category->name }}</a></div>
                            <a href="{{ route('post.single', ['slug' => $second_post->slug ])}}">
                              <h2 class="h4">{{ $second_post->title }}</h2></a>
                          </header>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                          <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                              <div class="avatar"><img src="{{ asset($second_post->user->profile->avatar)}}" alt="{{ $second_post->user->name }}" class="img-fluid"></div>
                              <div class="title"><span>{{ $second_post->user->name }}</span></div></a>
                            <div class="date"><i class="icon-clock"></i>{{ $second_post->created_at->diffForHumans() }}</div>
                            <div class="comments"><i class="icon-comment"></i>12</div>
                          </footer>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Post                            -->
                  <div class="row d-flex align-items-stretch">
                    <div class="text col-lg-7">
                      <div class="text-inner d-flex align-items-center">
                        <div class="content">
                          <header class="post-header">
                            <div class="category"><a href="{{ route('category.single', ['id' => $third_post->category->id ]) }}">{{ $third_post->category->name }}</a></div>
                                <a href="{{ route('post.single', ['slug' => $third_post->slug ])}}">
                              <h2 class="h4">{{ $third_post->title }}</h2></a>
                          </header>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                          <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                              <div class="avatar"><img src="{{ asset($third_post->user->profile->avatar)}}" alt="{{ $third_post->user->name }}" class="img-fluid"></div>
                              <div class="title"><span>{{ $third_post->user->name }}</span></div></a>
                            <div class="date"><i class="icon-clock"></i>{{ $third_post->created_at->diffForHumans() }}</div>
                            <div class="comments"><i class="icon-comment"></i>12</div>
                          </footer>
                        </div>
                      </div>
                    </div>
                    <div class="image col-lg-5"><img src="{{ $third_post->featured }}" alt="{{ $third_post->title }}"></div>
                  </div>
      </div>
    </section>
    <!-- Divider Section-->
    <section style="background: url(../blog/img/divider-bg.jpg); background-size: cover; background-position: center bottom" class="divider">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</h2><a href="#" class="hero-link">Veja mais...</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Latest Posts -->
    <section class="latest-posts">
      <div class="container">
        <header>
          <h2>Últimas do Blog</h2>
          <p class="text-big">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </header>
        <div class="row">
          <div class="post col-md-4">
            <div class="post-thumbnail"><a href="{{ route('post.single', ['slug' => $first_post->slug ])}}"><img src="{{ $first_post->featured }}" alt="{{ $first_post->title }}" class="img-fluid"></a></div>
            <div class="post-details">
              <div class="post-meta d-flex justify-content-between">
                <div class="date">{{ $first_post->created_at->toFormattedDateString() }}</div>
                <div class="category"><a href="{{ route('category.single', ['id' => $first_post->category->id ]) }}">{{ $first_post->category->name }}</a></div>
              </div><a href="{{ route('post.single', ['slug' => $first_post->slug ])}}">
                <h3 class="h4">{{ $first_post->title }}</h3></a>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
            </div>
          </div>
          <div class="post col-md-4">
            <div class="post-thumbnail"><a href="{{ route('post.single', ['slug' => $second_post->slug ])}}"><img src="{{ $second_post->featured }}" alt="{{ $second_post->title }}" class="img-fluid"></a></div>
            <div class="post-details">
              <div class="post-meta d-flex justify-content-between">
                <div class="date">{{ $second_post->created_at->locale('pt-br')->toFormattedDateString() }}</div>
                <div class="category"><a href="{{ route('category.single', ['id' => $second_post->category->id ]) }}">{{ $second_post->category->name }}</a></div>
              </div><a href="post.html">
                <h3 class="h4">{{ $second_post->title }}</h3></a>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
            </div>
          </div>
          <div class="post col-md-4">
            <div class="post-thumbnail"><a href="{{ route('post.single', ['slug' => $third_post->slug ])}}"><img src="{{ $third_post->featured }}" alt="{{ $third_post->title }}" class="img-fluid"></a></div>
            <div class="post-details">
              <div class="post-meta d-flex justify-content-between">
                <div class="date">{{ $third_post->created_at->toFormattedDateString() }}</div>
                <div class="category"><a href="{{ route('category.single', ['id' => $third_post->category->id ]) }}">{{ $third_post->category->name }}</a></div>
              </div><a href="post.html">
                <h3 class="h4">{{ $third_post->title }}</h3></a>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    @include('includes.subscrible')

    <!-- Gallery Section-->
    <section class="gallery no-padding">
      <div class="row">
        <div class="mix col-lg-3 col-md-3 col-sm-6">
          <div class="item"><a href="{{ asset('blog/img/gallery-1.jpg') }}" data-fancybox="gallery" class="image"><img src="{{ asset('blog/img/gallery-1.jpg') }}" alt="..." class="img-fluid">
              <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
        </div>
        <div class="mix col-lg-3 col-md-3 col-sm-6">
          <div class="item"><a href="{{ asset('blog/img/gallery-2.jpg') }}" data-fancybox="gallery" class="image"><img src="{{ asset('blog/img/gallery-2.jpg') }}" alt="..." class="img-fluid">
              <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
        </div>
        <div class="mix col-lg-3 col-md-3 col-sm-6">
          <div class="item"><a href="{{ asset('blog/img/gallery-3.jpg') }}" data-fancybox="gallery" class="image"><img src="{{ asset('blog/img/gallery-3.jpg') }}" alt="..." class="img-fluid">
              <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
        </div>
        <div class="mix col-lg-3 col-md-3 col-sm-6">
          <div class="item"><a href="{{ asset('blog/img/gallery-4.jpg') }}" data-fancybox="gallery" class="image"><img src="{{ asset('blog/img/gallery-4.jpg') }}" alt="..." class="img-fluid">
              <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
        </div>
      </div>
    </section>

    @include('layouts.footerblog')

    <!-- Overlay Search -->

@include('includes.search')

<!-- End Overlay Search -->

    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{ asset('blog/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('blog/vendor/jquery.cookie/jquery.cookie.js') }}"></script>
    <script src="{{ asset('blog/vendor/@fancyapps/fancybox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('blog/js/front.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script>
        @if(Session::has('subscribed'))
            toastr.success("{{ Session::get('subscribed')}}")
        @endif
    </script>

  </body>
</html>
