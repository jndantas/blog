<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'admin' => 1
        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.png',
            'about' => 'Lorem ipsum dolor sit amet, cosenctutu adipisich',
            'facebook' => 'https://facebook.com',
            'youtube' => 'https://youtube.com',
            'twitter' => 'https://twitter.com',
            'instagram' => 'https://instagram.com'
        ]);
    }
}
