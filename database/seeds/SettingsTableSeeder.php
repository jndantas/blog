<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'site_name' => "Laravel's Blog",
            'country' => 'Brasil, Bahia',
            'address' => 'Rua Qualquer',
            'contact_number' => '71 9999999999',
            'contact_email' => 'admin@admin.com.br',
            'disponible' => 'Seg-Sex 9-18',
            'support' => 'suporte online',
            'about' => 'Qolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibham
            liber tempor cum soluta nobis eleifend option congue nihil uarta decima et quinta.
            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl
            ut aliquip ex ea commodo consequat eleifend option nihil. Investigationes demonstraverunt
            lectores legere me lius quod ii legunt saepius parum claram.'
        ]);
    }
}
